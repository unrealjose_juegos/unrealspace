import java.util.Scanner;

public class Main {
	
	/*
	 * Version FINAL
	 */

	public static void main(String[] args) {
		
		//--Variables y Objetos--
		Nave Unreal = new Nave();
		Combate Combate = new Combate();
		Scanner sc = new Scanner(System.in);
		String Nombre;
		int Precio = 5;
		//--Fin Variables y Objeto--
		
		//--Introduccion--
		System.out.println("¿Como se llama su nave?");
		Nombre = sc.nextLine();
		Unreal.setNombre(Nombre);
		System.out.println("Bienvenido a bordo de la "+Unreal.getNombre());
		//--Fin Introduccion--
		
		//--Menu--
		while(true){
			System.out.println("Menu:\n1 -> Combate\n2 -> Reparaciones\n3 -> Inventario\n4 -> Estadisticas");
			int opcion = sc.nextInt();
			
			switch(opcion){
				case 1:
					if(!Unreal.getEstado()){
						Nave Enemigo = new Nave();
						Combate.Batalla(Unreal, Enemigo);
						break;
					}else{
						System.out.println("Necesitas reparar la nave");
						break;
					}				
				case 2:
					/*
					 * Si destruyen tu nave no podras volver a usarla hasta que la repares, cada vez sera mas caro repararla
					 */
					if(!Unreal.getEstado()){
						System.out.println("La "+Unreal.getNombre()+" esta en perfecto estado");
					}else{
						System.out.println("Su nave necesita una reparacion");
						System.out.println("¿Quieres reparar la nave? (Precio: "+Precio+" creditos)\n1 -> Si\n2 -> No");
						int eleccion = sc.nextInt();
						if(eleccion == 1){
							if(Unreal.getCreditos()>=Precio){
								Unreal.setCreditos(-Precio);
								Precio+=5;
								Unreal.setReparacion();
								System.out.println("Nave reparada");
							}else{
								System.out.println("No tienes suficientes creditos\nFIN DEL JUEGO");
							} // else
						} // if
					} // else
					break;
				case 3:
					Unreal.Inventario();
					break;
				case 4:
				    Unreal.Estadisticas();
				    break;
				default:
					break;
			} //Switch
		} //While (Menu)
		
		//--Fin Menu--

	} //Main

} //Clase Main