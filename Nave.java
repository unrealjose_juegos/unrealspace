public class Nave {
	
	/*
	 *  Version Final
	 */
	
	//----Creacion--
	
	public Nave(){
		Nombre = "Nave desconocida";
		Vida = 100;
		DañoLaser = 25;
		DañoCañon = 25;
		DañoMisil = 25;
		DefensaEscudo = 25;
		DefensaEvasion = 25;
		DefensaLaser = 25;
		Creditos = 0;
		NaveDestruida = false;
		Victorias = 0;
		Derrotas = 0;
	}
	
	//----Estadisticas--
	
	private double Vida;
	private double DañoLaser,DañoCañon,DañoMisil;
	private double DefensaEvasion,DefensaEscudo,DefensaLaser;
	private String Nombre;
	private int Creditos,Victorias,Derrotas;
	private boolean NaveDestruida;
	
	//----Nombre--
	
	public String getNombre(){
		return Nombre;
	}
	
	public void setNombre(String Nombre){
		this.Nombre = Nombre;
	}
	
	//----Vida--
	
	public double getVida(){
		return Vida;
	}
	
	//----Reparacion--
	
	public boolean getEstado(){
		return NaveDestruida;
	}
	
	public void setReparacion(){
		NaveDestruida = false;
	}
	
	public void setNaveDestruida(){
		NaveDestruida = true;
	}
	
	//----Armas y Defensas--
	
	public double getArmaLaser(){
		return DañoLaser;
	}
	
	public double getArmaCañon(){
		return DañoCañon;
	}
	
	public double getArmaMisil(){
		return DañoMisil;
	}
	
	public double getDefensaEscudo(){
		return DefensaEscudo;
	}
	
	public double getDefensaEvasion(){
		return DefensaEvasion;
	}
	
	public double getDefensaLaser(){
		return DefensaLaser;
	}
	
	//Estadisticas
	
	public int Victorias(){
	    return Victorias;
	}
	
	public int Derrotas(){
	    return Derrotas;
	}
	
	public void PartidaGanada(){
	    Victorias++;
	}
	
	public void PartidaPerdida(){
	    Derrotas++;
	}
	
	public void Estadisticas(){
	    System.out.println("Victorias de "+getNombre()+": "+Victorias);
	    System.out.println("Derrotas de "+getNombre()+": "+Derrotas);
	}
	
	//----Otros--------
	
	public int getCreditos(){
		return Creditos;
	}
	
	public void setCreditos(int oro){
		Creditos+=oro;
	}
	
	public void Inventario(){
		System.out.println("Creditos: "+getCreditos());
	}
}