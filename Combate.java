import java.util.Scanner;

/*
 * Vida = (int) Math.floor(Math.random()*(VidaMinima-VidaMaxima2+1)+VidaMaxima2);
 * */

/*
* Para evitar tener qe usar un break se podria intentar:
*   -Separar la creacion de pj del menu para en lugar de un break; poner un objeto.menu() por ejemplo
*   -¿?
*/

public class Combate {

	public void Batalla(Nave Unreal,Nave Enemigo){
		
		//--Variables--
		double Daño,Escudo,DañoEnemigo,EscudoEnemigo;
		int Arma,Defensa,ArmaEnemigo,DefensaEnemigo;
		int turno = 0,oro;
		double Vida,VidaEnemigo;
		Vida = 	Unreal.getVida();
		VidaEnemigo = Enemigo.getVida();
		//--Fin Variables--
		
		while(true) {
			
			turno++;
			System.out.println();
			System.out.println("#########\n#Turno "+turno+"#\n#########");
			
			System.out.println();
			System.out.println("###################################################################");
			System.out.println("Nombre: "+Unreal.getNombre()+"\nVida: ("+Vida+"/"+Unreal.getVida()+")");
			System.out.println("Nombre: "+Enemigo.getNombre()+"\nVida: ("+VidaEnemigo+"/"+Enemigo.getVida()+")");
			System.out.println("###################################################################");
			
			int arma = ElegirArma();
			
			switch(arma){
				case 1:
					Daño = Unreal.getArmaLaser();
					Arma = 1;
					break;
				case 2:
					Daño = Unreal.getArmaCañon();
					Arma = 2;
					break;
				case 3:
					Daño = Unreal.getArmaMisil();
					Arma = 3;
					break;
				default:
					Daño = 0;
					Arma = 0;
					break;
			}
			
			int defensa = ElegirDefensa();
			
			switch(defensa){
				case 1:
					Escudo = Unreal.getDefensaEscudo();
					Defensa = 1;
					break;
				case 2:
					Escudo = Unreal.getDefensaEvasion();
					Defensa = 2;
					
					break;
				case 3:
					Escudo = Unreal.getDefensaLaser();
					Defensa = 3;
					break;
				default:
					Defensa = 0;
					Escudo = 0;
					break;
			}
			
			int armaEnemigo = (int) Math.floor(Math.random()*(0-4+1)+4); //Lo valores minimoy maximo no entran
			int defensaEnemigo = (int) Math.floor(Math.random()*(0-4+1)+4);
			
			switch(armaEnemigo){
				case 1:
					DañoEnemigo = Enemigo.getArmaLaser();
					ArmaEnemigo = 1;
					break;
				case 2:
					DañoEnemigo = Enemigo.getArmaCañon();
					ArmaEnemigo = 2;
					break;
				case 3:
					DañoEnemigo = Enemigo.getArmaMisil();
					ArmaEnemigo = 3;
					break;
				default:
					DañoEnemigo = 0;
					ArmaEnemigo = 0;
					break;
			}
			
			switch(defensaEnemigo){
				case 1:
					EscudoEnemigo = Enemigo.getDefensaEscudo();
					DefensaEnemigo = 1;
					break;
				case 2:
					EscudoEnemigo = Enemigo.getDefensaEvasion();
					DefensaEnemigo = 2;
					break;
				case 3:
					EscudoEnemigo = Enemigo.getDefensaLaser();
					DefensaEnemigo = 3;
					break;
				default:
					DefensaEnemigo = 0;
					ArmaEnemigo = 0;
					break;
			}
	
			if(Arma == 0) System.out.println(Unreal.getNombre()+" no hace nada");
			else if(Arma == 1) System.out.println(Unreal.getNombre()+" dispara los laser");
			else if(Arma == 2) System.out.println(Unreal.getNombre()+" dispara los cañones");
			else if(Arma == 3) System.out.println(Unreal.getNombre()+" dispara los misiles");
			
			if(DefensaEnemigo == 1) System.out.println(Enemigo.getNombre()+" activa los escudos");
			else if(DefensaEnemigo == 2) System.out.println(Enemigo.getNombre()+" activa el modulo de evasion");
			else if(DefensaEnemigo == 3) System.out.println(Enemigo.getNombre()+" activa los sistemas antimisiles");
			
			if (Arma == DefensaEnemigo){
				System.out.println(Enemigo.getNombre()+" bloqueo el ataque");
			}else{
				System.out.println(Unreal.getNombre()+" infligio "+Daño+" de daño a "+Enemigo.getNombre());
				VidaEnemigo-=Daño;
			}
			
			//---Muerte Enemigo------
			if(VidaEnemigo<=0){
				System.out.println(Enemigo.getNombre()+" ha sido eliminado por "+Unreal.getNombre());
				oro = (int) Math.floor(Math.random()*(-1-4+1)+5)+turno;
				Unreal.setCreditos(oro);
				System.out.println(Unreal.getNombre()+" ha obtenido "+oro+" creditos");
				Unreal.PartidaGanada();
				break;
			}
			//-----------------------
			
			System.out.println();
			
			if(ArmaEnemigo == 1) System.out.println(Enemigo.getNombre()+" dispara los laser");
			else if(ArmaEnemigo == 2) System.out.println(Enemigo.getNombre()+" dispara los cañones");
			else if(ArmaEnemigo == 3) System.out.println(Enemigo.getNombre()+" dispara los misiles");
			
			if(Defensa == 1) System.out.println(Unreal.getNombre()+" activa los escudos");
			else if(Defensa == 2) System.out.println(Unreal.getNombre()+" activa el modulo de evasion");
			else if(Defensa == 3) System.out.println(Unreal.getNombre()+" activa los sistemas antimisiles");
						
			if (ArmaEnemigo == Defensa){
				System.out.println(Unreal.getNombre()+" bloqueo el ataque");
			}else{
				System.out.println(Enemigo.getNombre()+" infligio "+DañoEnemigo+" de daño a "+Unreal.getNombre());
				Vida-=DañoEnemigo;
			}
			
			//----Muerte Jugador-----
			if(Vida<=0){
				System.out.println("Tu nave ha sido eliminada");
				Unreal.setNaveDestruida();
			    Unreal.PartidaPerdida();
				break;
			}
			//-----------------------
			
		}
		
	}
	
	public int ElegirArma(){
		Scanner sc = new Scanner(System.in);
		System.out.println("1 -> Laser\n2 -> Cañon\n3 -> Misil");
		int eleccion = sc.nextInt();	
		if(eleccion>4 & eleccion<=0){
			eleccion = ElegirArma();
		}
		return eleccion;
	}
	
	public int ElegirDefensa(){
		Scanner sc = new Scanner(System.in);
		System.out.println("1 -> Evasion\n2 -> Escudo\n3 -> Laser");
		int eleccion = sc.nextInt();
		if(eleccion>4 & eleccion<=0){
			eleccion = ElegirDefensa();
		}
		return eleccion;
	}
	
}
